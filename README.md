# ECE651 HW2 README

## Compiling
To compile this, run:

### Windows
`javac src\Person.java`

`javac src\BlueDevil.java`

`javac src\Execute.java`



### Linux / OSX
`cd src`

`javac Person.java`

`javac BlueDevil.java`

`javac Execute.java`




## Running
To run this code, use:
### Linux / OSX
`java Execute`
### Windows
`java Execute`

Follow the prompt on the screen to interact
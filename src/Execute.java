import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class Execute {
	//store all objects
	static ArrayList<BlueDevil> list=new ArrayList<BlueDevil>();
	//store all objects and make sure the id is unique
	static Map<String, BlueDevil> map=new HashMap<String, BlueDevil>();
	public static void addRecord(String name, int age, String id, String position) {
		BlueDevil toAdd=new BlueDevil();
		toAdd.setName(name);
		toAdd.setAge(age);
		toAdd.setId(id);
		toAdd.setPosition(position);
		if(map.put(id, toAdd)!=null) {
			
			System.out.println("This id is already exist");
			return;
		}
		list.add(toAdd);
		
		System.out.println("Successfully add!");
		
	}
    public static String whoIs(ArrayList<BlueDevil> list, String name) {
    	
    	//return result of this function
    	String res="";
        //check whether the name can be found or not
    	int found=0;
    	for(BlueDevil item : list ) {
    		//if found the person
    		if(item.getName().equals(name)) {
    			found=1;
    			res=item.getName()+" is "+item.getAge()+" years old"+". "+name+" is a "+item.position+" in Duke.";
    			break;
    		}
    	}
    	//if not found the person
    	if(found==0) res="not found this person "+name;
    	return res; 
    }
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BlueDevil[] persons=new BlueDevil[4];
		for(int i=0; i<persons.length; i++) {
			persons[i]=new BlueDevil();
			list.add(persons[i]);
		}
		//initialize name for objects
		persons[0].setName("Adel Fahmy");
		persons[1].setName("Yuanyuan Yu");
		persons[2].setName("You Lyu");
		persons[3].setName("Yijie Yan");
		//initialize age for objects
		persons[0].setAge(40);
		persons[1].setAge(23);
		persons[2].setAge(23);
		persons[3].setAge(22);
		//initialize id
		persons[0].setId("1");
		persons[1].setId("2");
		persons[2].setId("3");
		persons[3].setId("4");
		
		//initialize position for objects
		persons[0].setPosition("professor");
		persons[1].setPosition("TA");
		persons[2].setPosition("TA");
		persons[3].setPosition("student");
		
		for (BlueDevil item : list) {
			map.put(item.getId(),item);
		}
		Scanner input=new Scanner(System.in);
		while(true) {
		System.out.println("#######################################");
		System.out.println("Please select the function as following");
		System.out.println("1. Search people");
		System.out.println("2. Add people record");
		System.out.println("3. Exit");
		System.out.println("Please input the number of function you want");
		//read user input from console as function number
		
		String option=(String)input.nextLine();
		switch(option){
		case "1":
			System.out.println("Please enter the name you want to search (case sensitive)");
			String toSearch=input.nextLine();
			String result=whoIs(list, toSearch);
			System.out.println(result);
			
			break;
		case "2":
			System.out.println("Adding record, please follow the  instructions to add.");
			System.out.println("Please enter the name");
			String name=input.nextLine();
			System.out.println("Please enter the age");
			String ageS=input.nextLine();
			int age=Integer.parseInt(ageS);
			System.out.println("Please enter the id");
			String id=input.nextLine();
			System.out.println("Please enter the position");
			String position=input.nextLine();
			addRecord(name, age, id, position);
			
			break;
			
		case "3":
			//exit the program
			break;
			
		default:
			System.out.println("Please enter the right number");
			break;
			
		}
		
		
		if(option.contentEquals("3")) {
			
			System.out.println("Exiting!");
			break;
		}
		
		
	}
		input.close();
	}

}
